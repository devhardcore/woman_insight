var express = require('express');
var router = express.Router();

router.get('/feed', function(req, res, next) {
  res.render('pages/feed', {});
});
router.get('/feed/inspiration/single', function(req, res, next) {
  res.render('pages/feed-inspiration-single', {});
});
router.get('/feed/articles/single', function(req, res, next) {
  res.render('pages/feed-articles-single', {});
});
router.get('/feed/videos/single', function(req, res, next) {
  res.render('pages/feed-videos-single', {});
});
router.get('/feed/live/single', function(req, res, next) {
  res.render('pages/feed-live-single', {});
});

router.get('/notifications', function(req, res, next) {
  res.render('pages/notifications', {});
});

router.get('/consultations', function(req, res, next) {
  res.render('pages/consultations', {});
});
router.get('/consultations/single', function(req, res, next) {
  res.render('pages/consultations-single', {});
});

router.get('/profile', function(req, res, next) {
  res.render('pages/profile', {});
});
router.get('/profile/payments', function(req, res, next) {
  res.render('pages/profile-payments', {});
});
router.get('/profile/payments-history', function(req, res, next) {
  res.render('pages/profile-payments-history', {});
});
router.get('/profile/notifications', function(req, res, next) {
  res.render('pages/profile-notifications', {});
});

router.get('/login', function(req, res, next) {
  res.render('pages/login', {});
});
router.get('/registration', function(req, res, next) {
  res.render('pages/registration', {});
});

module.exports = router;
